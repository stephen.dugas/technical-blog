---
title: Technical Overview
permalink: /docs/tech-overview/
---

## Getting started

Below you should find different resources that developers at 
different levels should be looking into and understanding more about.
If resources are available for the topic, feel free to add them.

### Basic Concepts

 * Interface Design 
 * Threading 
 * Dynamic vs Strict Typing 
 * Service Repository Pattern 
 * Unit Testing 
 * MVC 
 * MVVM 
 * ORM 
 * Dependency Injection 
 * SOLID 
 * GRASP 
 * Logging


### Intermediate Concepts

 * 12 Factor Apps 
 * Functional Basics 
 * Reflection 
 * XSD and XML 
 * Json Schema and JSON 
 * API Design and Backwards Compatibility 
 * JIT vs Pre-Compiled 
 * MEF or Plugin Loading 
 * Continuous Integration and Continuous Delivery 
 * Integration Testing / System Testing 
 * Package Management (NPM, Nuget) 
 * Basic Firewalls and Network Pathing 
 * Queueing basics 
 * Attributes 