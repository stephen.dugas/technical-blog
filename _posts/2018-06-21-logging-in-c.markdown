---
layout: post
title: "Logging in C#"
date: "2018-06-21 14:39:48 -0400"
author: Jonathan Cockrem
---


## Introduction

Building applications without diagnostic capabilities is a thing of the past. Today we have everything from integrated testing frameworks to CI runners in automated testing situations to help diagnose or demonstrate proper application operation. While on the pursuit to educate myself on the different logging or tracing options are available, I will be documenting some different aspects of my findings and offer some samples.

Implementing a logging system is a great way to offer an external repository or an screen (Console output) printing of your applications path of execution. With minor adjustments it can be made more or less verbose depending on your needs. This can allow for greater detail available to inspect or troubleshoot the application.

### Advantages

 The importance of logging in software is apparent to developers who have implemented it or have worked along side it. Logging can be a reflection of the state of an application in terms of the execution path and the specific steps a program takes during its execution. This includes offering insight into your application, as it goes through all the important steps of the program. Of course, the information being logged can be more, or less, verbose depending on the implementation of the logging. 
 
 For a better debugging experience, the more detailed information is favoured, alternatively you can offer less technical information and more instructional information to help users help themselves.

 Since once it is set up and working properly, it is unlikely to fail. This level of automation is important for the accuracy and consistency of generating of the log files.
<sup>[2]</sup>

### Disadvantages

 There are a few drawbacks to logging, not everything is perfect. Some logging solutions can be incredibly complex and a real pain to implement. Additionally, there are some situations that make it a little bit more complicated to utilize, depending on the configuration and project structure.

 For example, we were logging our library project but wanted our application accessing the library to log for the library as well. We ended up using a more complicated TraceSource solution to accomplish this, however it was not as intuitive as some of the more simple logging options.

 Another issue that can arise is the resources being consumed for the logging.

 If anything were to be improperly modified later on, could cause issues with valuable logging data.

 When a logging system is implemented, if it has not been configured properly, you may not be getting all the data in your log that you require. This lack of important data could prevent proper troubleshooting or debugging.
<sup>[2]</sup>

### Sample

I am going to be posting about two technologies available, the integrated `System.Diagnostics` tracing functionality, as well as the open source logging software NLog to demonstrate the similar abilities and features of an 'Out of the box' solution and a 3rd party solution.

I will preface this by saying this will have some sample code or examples provided to set up basic logging process.


#### Trace Class

For the Trace Class to log properly, we first need to configure the App.config file to add the listeners and the log output file name.<sup>[4]</sup>
This config file shows us that the listener we created is called **testListener**

```xml
<?xml version="1.0" encoding="utf-8" ?>
<configuration>
    <startup>
        <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.6.1" />
    </startup>
  <!-- Config of Trace -->
  <system.diagnostics>
    <trace autoflush="true" indentsize="4">
      <listeners>
        <add name="testListener" type="System.Diagnostics.TextWriterTraceListener" initializeData="TextWriterOutput.log" />
        <remove name="Default" />
      </listeners>
    </trace>
  </system.diagnostics>
  <!-- End of Config-->
</configuration>
```

Along with this configuration file, we can also now add the Trace functionality to the project. Across any classes that implement the appropriate syntax as well. Take this switch case as an example. Fortunately a very simple syntax, and very obvious how it acts. 

I have three classifications I used to help logging:
+ I have used `Trace.TraceInformation`, to let you know where abouts in the applications execution you are located. 
+ Secondly, I have `Trace.TraceWarning` indicating whether or not there is potential for an error. 
+ Finally , I have used the `Trace.TraceError` to indicate a fatal error or something that stops the normal operation of the code. 

These three levels help someone reading a log to properly assess the issue and if done correctly should help with debugging for other developers of be high level enough for users to properly troubleshoot their own issues.<sup>[4]</sup>

<br/>

##### Required imports
We will need to import `System.Diagnostics` at the top of the class to use the Trace Class.

```c#
using System;
using System.Diagnostics;
```

<br/>
##### Common trace functions

Sample of different Trace functions:

```c#
switch (selection)
{
    case 1:
     Trace.TraceInformation("Switch Case 1");

      Multiply m = new Multiply();

      Trace.TraceWarning("Object May Be Null Warning");
      if(m == null)
      {
        Trace.TraceError("Object is Null Error");
         Console.WriteLine("Object is null");
      }

      Console.WriteLine(m.Mult(firstNumber, secondNumber));
         break;

     case 2:
        Trace.TraceInformation("Switch Case 2");

        Divide d = new Divide();
        Console.WriteLine(d.Div(firstNumber, secondNumber));
          break;

     default:
        Trace.TraceInformation("Switch Case Default");

         Console.WriteLine("Nothing here");
         running = false;
           break;
}
```

<br/>

##### Trace Class output

Sample log output from this following function:

```c#
    public int Div(int num1, int num2)
    {
        Trace.TraceInformation("Div: First Parameter " + num1 + " Second Paramenter : " + num2);
        Trace.Flush();
        int total;

        Console.WriteLine("Divide " + num1 + " and " + num2);

        total = num1 / num2;

        Trace.TraceInformation("Div : Total = " + total);
        Trace.Flush();

        return total;
    }
```

The log file contents:

```
TraceLog.exe Information: 0 : Div: First Parameter 50 Second Paramenter : 10
TraceLog.exe Information: 0 : Div: Total = 5
```
<br/>

#### NLog

 To install NLog in a Visual Studio (Visual Studio Community 2017 is what I am using for this demonstration) environment, I merely went to Solution Explorer, right click on the project, select *Manage Nuget Packages for Solution* which will then open the package explorer. Select the Browse tab and search for NLog. The top result is for NLog at v 4.5.6, next to the version of the package should be an 'Install' button which will promptly add the package to your project.
 At the top of your class, you will need to import NLog like so.

 Just like Trace Class, there are different levels of logging:
+ Trace - Highly detailed logs with lots of information, typically only used during development.
+ Debug - Offers debugging information, less detailed than Trace, not often used in production environment.
+ Info - Basic execution information, often enabled in production.
+ Warn - Warning messages , non-critical issues that can be recovered or can be temporary failures.
+ Error - Error messages , these can be exceptions.
+ Fatal - Critical errors that end the application.

<br/>

##### Required imports

Required imports for NLog library

```c#
using System;
using System.Diagnostics;
using NLog;
```

<br/>
##### Configuring NLog
 NLog can be configured in an `NLog.config` file just like the Tracer class. The following is an XML <sup>[1]</sup>:

```xml
<?xml version="1.0" encoding="utf-8" ?>
<nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <!-- NLog config target start -->
  <targets>
    <target name="logfile" xsi:type="File" fileName="testNLog.txt" />
    <target name="logconsole" xsi:type="Console" />
  </targets>
  <!-- End of NLog target config-->
  <rules>
    <logger name="*" minlevel="Info" writeTo="logconsole" />
    <logger name="*" minlevel="Debug" writeTo="logfile" />
  </rules>
</nlog>
```

<br/>
##### NLog can be also implemented in-line

instead of in an `NLog.config` file, which offers an alternative method for implementation<sup>[1]</sup>:

```c#
//Configure NLog programmatically, instead of NLog.config file
var config = new NLog.Config.LoggingConfiguration();
var logFile = new NLog.Targets.FileTarget("logfile")
{
    FileName = "file.txt"
};

config.AddRule(LogLevel.Debug, LogLevel.Fatal, logFile);
NLog.LogManager.Configuration = config;

var logger = NLog.LogManager.GetCurrentClassLogger();
logger.Info("Test NLog");
```
<br/>
##### NLog output
Sample log output from this following function:

```c#
    public int Mult(int num1, int num2)
    {
        var logger = LogManager.GetCurrentClassLogger();

        logger.Info("Mult : First Param " + num1 + " Second Param " + num2);
        int total;
        total = num1* num2;
        logger.Info("Mult : Total = " + total);

        if(total == 0)
        {
            logger.Error("Mult : ERROR Total is equal to 0");
            return 0;
        }
        return total;
    }
```

The log file contents:

```
2018-06-25 11:27:12.8915|INFO|TraceLog.Multiply|Mult : First Param 8 Second Param 6
2018-06-25 11:27:12.9226|INFO|TraceLog.Multiply|Mult : Total = 48
```

###  Options and Availability

 In the market of logging, there are many different types of systems available. People can choose to use the integrated logging systems available in their framework, or search for an open-source alternative or maybe opt for a paid on-prem commercial solution. These different options work on a range of solutions from small single class console applications to full blow web applications that serve many users/clients. Regardless of the application of the logging, it is an important aspect of software development.

#### Alternatives 
 Open Source logging for your own personal curiosity<sup>[3]</sup> :
 * log4net  
 * CSharp Logger
 * CShap Dot Net Logger
 * Logger.Net
 * LogThis
 * TraceTool
 * Common.Logging
 * Elmah

### Conclusion 
Both of these logging systems output to files, however, both of these systems can be configured to print out to the Console.
The advantages of this would be to get the logging information much more quickly and while the application is running you can see the tracing of the execution that will either confirm or refute that the application will be operating properly.


They have different formats and can be formatted to display different information or to rearrange some of the information to suit your needs more specifically.


[1]: https://github.com/NLog/NLog/wiki/Tutorial
[2]: https://blog.rapid7.com/2014/09/05/the-pros-and-cons-of-open-source-logging/
[3]: https://csharp-source.net/open-source/logging
[4]: https://docs.microsoft.com/en-us/dotnet/framework/debug-trace-profile/how-to-create-and-initialize-trace-listeners
