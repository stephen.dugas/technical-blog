---
layout: post
title:  "Intro Post"
date:   2018-05-08 21:40:09
author: Daniel
---
Our first post on the technical blog. This blog is done up in jekyll and hosted on the gitlab pages. The blog is autodeployed using gitlab-ci.
You can do cool code highlighting like below

{% highlight c# %}
public class Program
{
  public static void (string[] args)
  {
    Console.WriteLine("Hello World");
  }
}
{% endhighlight %}

Check out the [Jekyll docs][jekyll] for more info on how to get the most out of Jekyll. File all bugs/feature requests at [Jekyll’s GitHub repo][jekyll-gh]. If you have questions, you can ask them on [Jekyll’s dedicated Help repository][jekyll-help].

[jekyll]:      http://jekyllrb.com
[jekyll-gh]:   https://github.com/jekyll/jekyll
[jekyll-help]: https://github.com/jekyll/jekyll-help
